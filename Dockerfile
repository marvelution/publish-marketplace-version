FROM adoptopenjdk/openjdk11:x86_64-ubuntu-jdk-11.0.5_10-slim

COPY pipe/target/publish-marketplace-version-pipe-docker/* /usr/pipe/

WORKDIR /usr/pipe/

CMD java -cp "/usr/pipe/*" org.marvelution.buildsupport.PublishToMarketplace
