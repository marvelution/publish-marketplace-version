# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.0.6

- patch: Log exception details

## 0.0.5

- patch: Only log if publishing fails

## 0.0.4

- patch: Improve debug logging

## 0.0.3

- patch: Improvements and fixes to publish data center versions

## 0.0.2

- patch: Support for updating payment model

## 0.0.1

- patch: Initial Release

