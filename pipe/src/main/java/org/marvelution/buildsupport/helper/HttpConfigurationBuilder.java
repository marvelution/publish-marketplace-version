/*
 * Copyright (c) 2019-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.buildsupport.helper;

import org.marvelution.buildsupport.*;

import com.atlassian.marketplace.client.http.*;

import static com.atlassian.marketplace.client.http.HttpConfiguration.*;
import static io.atlassian.fugue.Option.*;
import static java.lang.String.*;

/**
 * Builder for {@link HttpConfiguration}.
 *
 * @author Mark Rekveld
 */
public class HttpConfigurationBuilder
{

	private static final int TIMEOUT = 50000;
	private static final String USERNAME_KEY_FORMAT = "%S_USER";
	private static final String TOKEN_KEY_FORMAT = "%S_TOKEN";

	static HttpConfiguration build(
			PipeConfiguration configuration,
			String service)
	{
		HttpConfiguration.Builder builder = builder().connectTimeoutMillis(TIMEOUT).readTimeoutMillis(TIMEOUT);
		configuration.getVariable(format(USERNAME_KEY_FORMAT, service)).ifPresent(username -> {
			String token = format(TOKEN_KEY_FORMAT, service);
			builder.credentials(some(new HttpConfiguration.Credentials(username, configuration.requireVariable(token))));
		});
		return builder.build();
	}
}
