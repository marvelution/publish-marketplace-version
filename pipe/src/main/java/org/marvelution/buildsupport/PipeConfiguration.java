/*
 * Copyright (c) 2019-present Marvelution B.V.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.marvelution.buildsupport;

import java.net.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

import com.atlassian.marketplace.client.api.*;
import org.apache.commons.lang3.*;

/**
 * Pipe Configuration holder.
 *
 * @author Mark Rekveld
 */
public class PipeConfiguration
{

	private final UnaryOperator<String> operator;

	public PipeConfiguration()
	{
		this(System::getenv);
	}

	public PipeConfiguration(UnaryOperator<String> operator)
	{
		this.operator = operator;
	}

	public Optional<URI> getUri(String key)
	{
		return getVariable(key).map(URI::create);
	}

	public Optional<Boolean> getBoolean(String key)
	{
		return getVariable(key).map(Boolean::parseBoolean);
	}

	public <E extends Enum<E>> Optional<E> getEnum(
			String key,
			Class<E> type)
	{
		E[] values = type.getEnumConstants();
		return getVariable(key).flatMap(v -> Stream.of(values)
				.filter(e -> e.name().equalsIgnoreCase(v) || (e instanceof EnumWithKey && ((EnumWithKey) e).getKey().equalsIgnoreCase(v)))
				.findFirst());
	}

	public String requireVariable(String key)
	{
		return getVariable(key).orElseThrow(() -> new IllegalArgumentException("missing " + key + " variable"));
	}

	public Optional<String> getVariable(String key)
	{
		return Optional.ofNullable(operator.apply(key)).filter(StringUtils::isNotBlank);
	}
}
