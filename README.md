# Bitbucket Pipelines Pipe: publish-marketplace-version

Publish a new app version on the Atlassian Marketplace, optionally including release details from Jira.
**Works with Jira Server, Data Center and Cloud**

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: marvelution/publish-marketplace-version:0.0.6
  variables:       
    MARKETPLACE_BASE_URL: '<string>' # Optional.
    MARKETPLACE_USER: '<string>'
    MARKETPLACE_TOKEN: '<string>'         
    VERSION_ARTIFACT: '<string>'
    VERSION_STATUS: '<string>' # Optional.
    VERSION_PAYMENT_MODEL: '<string>' # Optional.
    JIRA_BASE_URL: '<string>' # Optional.
    JIRA_API_USER: '<string>' # Optional.
    JIRA_API_TOKEN: '<string>' # Optional, but required if JIRA_API_USER is set.
    JIRA_PROJECT_KEY: '<string>' # Optional.
    JIRA_VERSION_FORMAT: '<string>' # Optional.
    ISSUE_SECURITY_LEVEL_FILTER: <boolean> # Optional.
    DEBUG: <boolean> # Optional.
```

## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| MARKETPLACE_BASE_URL | URL of the Marketplace instance, `https://marketplace.atlassian.com` by default. |
| MARKETPLACE_USER (*) | Username of the Atlassian account for Authentication. Example: `human` or `human@example.com` |
| MARKETPLACE_TOKEN (*) | Password of the Atlassian account for Authentication. |
| VERSION_ARTIFACT (*) | The new version file to upload to the Marketplace. |
| VERSION_STATUS | The version status, `public` or `private`, to publish the version as, `public` by default. |
| VERSION_PAYMENT_MODEL | The version payment model, either `free`, `vendor` or `atlassian`. `free` by default. |
| JIRA_BASE_URL | URL of Jira instance. Example: `https://<yourdomain>.atlassian.net` or `https://issues.<yourdomain>.com` |
| JIRA_API_USER | Username (Jira Server/Data Center) or Email address (Jira Cloud). Example: `human` or `human@example.com` |
| JIRA_API_TOKEN | **Password**/**Access Token** for Authorization. Example: `HXe8DGg1iJd2AopzyxkFB7F2` ([Jira Cloud How To](https://confluence.atlassian.com/cloud/api-tokens-938839638.html)) |
| JIRA_PROJECT_KEY | Key of the Jira project that contains the version that should be released. |
| JIRA_VERSION_FORMAT | Format used to get the Jira version name from the artifact version, `%s` by default. Example: `server-%s`. |
| ADDITIONAL_JQL | Additional JQL to collect the Jira issues associated with the add-on version for release notes. Fields `projectKey`, `fixVersion` and `statusCategory` are added automatically. |
| ISSUE_SECURITY_LEVEL_FILTER | Turn on to hide issues from the release notes that have a security level set, equivalent to JQL `level is EMPTY`. `false` by default. |
| DEBUG | Turn on extra debug information. `false` by default. |

_(*) = required variable._

## Details

This pipe will publish a new version of an app by uploading it's artifact and using the previous version as a base for the new version.
 It will optionally include release notes from Jira if Jira settings are also provided.

## Prerequisites

To publish a new version on a Marketplace, you first need to have an initial version of the app already published. Since this initial
 version is used to base the new version on copying information like screenshots, links, compatibilities and more.

To get release notes from Jira Cloud, you need to create a Personal access token. You can follow the instructions
 [here](https://confluence.atlassian.com/cloud/api-tokens-938839638.html) to create one.

## Examples

Basic example:

```yaml
script:
  - pipe: marvelution/publish-marketplace-version:0.0.6
    variables:
      MARKETPLACE_USER: $MARKETPLACE_USER
      MARKETPLACE_TOKEN: $MARKETPLACE_TOKEN
      VERSION_ARTIFACT: "target/my-addon-1.0.0.obr"
```

Example with Jira provided release notes:

```yaml
script:
  - pipe: marvelution/publish-marketplace-version:0.0.6
    variables:
      MARKETPLACE_USER: $MARKETPLACE_USER
      MARKETPLACE_TOKEN: $MARKETPLACE_TOKEN
      VERSION_ARTIFACT: "**/my-addon-1.0.0.obr"    
      VERSION_STATUS: 'private'
      VERSION_PAYMENT_MODEL: 'atlassian'
      JIRA_BASE_URL: $JIRA_BASE_URL
      JIRA_API_USER: $JIRA_API_USER_EMAIL
      JIRA_API_TOKEN: $JIRA_API_TOKEN
      JIRA_PROJECT_KEY: "BP"
      JIRA_VERSION_FORMAT: "server-%s"
      ADDITIONAL_JQL: "category = Open-Source"
      ISSUE_SECURITY_LEVEL_FILTER: "true"
```

## Support

If you'd like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you're reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License

Copyright (c) 2019 Marvelution.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[community]: https://community.atlassian.com/t5/forums/postpage/choose-node/true/interaction-style/qanda?add-tags=bitbucket-pipelines,pipes,jira
